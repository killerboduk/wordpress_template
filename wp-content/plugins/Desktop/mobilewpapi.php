<?php

/**
 * Plugin Name: mobilewpapi
 * Plugin URI:  
 * Version:     1.0
 * Description: <a target='_blank' href='../wp-content/plugins/mobilewpapi/'> index! </a>
 * Author:      <a target='_blank' href='http://gregoriobalonzo.ml'>http://gregoriobalonzo.ml</a>
 * Author URI:  
 * License:     GPL2
 * License URI: https://www.gnu.org/licenses/gpl-2.0.html
 * Text Domain: gregoriobalonzo.ml
 */

date_default_timezone_set("Asia/Manila"); 
define('MOBILEWPAPI',plugin_dir_path(__FILE__));
define('PLUGINNAME','mobilewpapi');
define('PLUGINURL','/wp-content/plugins/mobilewpapi');