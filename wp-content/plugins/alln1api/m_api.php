<?php
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Headers: *");
header('Access-Control-Max-Age: 1000');
header("Access-Control-Allow-Methods: GET, POST");
// ini_set('display_errors', 1);
// ini_set('display_startup_errors', 1);
// error_reporting(E_ALL);
?>
<?php

require_once("../../../wp-load.php");
global $wpdb;

/**
*
* MOBILE LOGIN
**/

if(isset($_POST['username']) && isset($_POST['password'])){

       $username = sanitize_user($_POST['username']);
       $password = sanitize_user(trim($_POST['password']));
       $user = apply_filters( 'authenticate', null, $username, $password );

    if ( $user == null ) {
        // TODO what should the error message be? (Or would these even happen?)
        // Only needed if all authentication handlers fail to return anything.
        $user = new WP_Error( 'authentication_failed', __( '<strong>ERROR</strong>: Invalid username, email address or incorrect password.' ) );
    }

    $ignore_codes = array( 'empty_username', 'empty_password' );

    if ( is_wp_error( $user ) && ! in_array( $user->get_error_code(), $ignore_codes ) ) {

    }else{
            wp_set_current_user($user->ID, $user->user_login);
            wp_set_auth_cookie($user->ID);

    }
    if($user->get_error_code() == ''){
        echo 0;
    }else{
        echo 1;
    }
}

/*
*
* MOBILE ORDER
*/

if(isset($_POST['order'])){

// $id = sanitize_user($_GET['id']);
//   global $woocommerce;
//   $user = get_current_user_id();

//   $address = array(
//       'first_name' => 'Joe',
//       'last_name'  => 'Conlin',
//       'company'    => 'Speed Society',
//       'email'      => 'joe@testing.com',
//       'phone'      => '760-555-1212',
//       'address_1'  => '123 Main st.',
//       'address_2'  => '104',
//       'city'       => 'San Diego',
//       'state'      => 'Ca',
//       'postcode'   => '92121',
//       'country'    => 'US'
//   );

//   // Now we create the order
//   $order = wc_create_order();

//   // The add_product() function below is located in /plugins/woocommerce/includes/abstracts/abstract_wc_order.php
//   $order->add_product( get_product( $id ), $user ); // This is an existing SIMPLE product
//   $order->set_address( $address, 'billing' );
//   //
//   $order->calculate_totals();
//   $order->update_status("Completed", 'Imported order', TRUE);
//   return 1;
//    $user_id = 1;
//    $awesome_level = 1000;
//    add_user_meta( $user_id, '_level_of_awesomeness', $awesome_level,true);
//    $user_last = get_user_meta( $user_id, '_level_of_awesomeness', $single );
//    print_r($user_last);
//    //echo json_encode(wp_get_current_user());
}

if(isset($_GET['currentuser'])){
    echo wp_get_current_user()->ID;
}
if(isset($_GET['user'])){
    $user = sanitize_user($_GET['user']);
    global $wpdb;
    $sql = $wpdb->get_results("SELECT *
        FROM wp_users,wp_usermeta
        WHERE wp_users.ID = wp_usermeta.user_id
        && wp_users.ID= '$user' ");
        $userData = array();
        foreach($sql as $data){
            $userData[$data->meta_key] = $data->meta_value;
        }
        echo json_encode($userData);
}

if(isset($_GET['orders']))
{

    if (!class_exists('WooCommerce')) :
        require ABSPATH . 'wp-content/plugins/woocommerce/woocommerce.php';
        $orders = get_all_orders();
    endif;

    function get_all_orders() {
        $user_id = $_GET['orders'];
        $customer_orders = get_posts(apply_filters('woocommerce_my_account_my_orders_query', array(
            'numberposts' => -1,
            'meta_key' => '_customer_user',
            'meta_value' => $user_id,
            'post_type' => wc_get_order_types('view-orders'),
            'post_status' => array_keys(wc_get_order_statuses())
        )));
        return $customer_orders;
    }
    $order = get_all_orders();
    $order_array = array();
    foreach($order as $value)
    {
        $order_array['ID'] = $value->ID;
        $order_array['post_status'] = $value->post_status;
        $order_array['post_name'] = $value->post_name;
    }
    print_r($order);
}

if(isset($_GET['currencysymbol']))
{
    echo get_option('woocommerce_currency');
}

if(isset($_GET['productimage']))
{
    $id = sanitize_user($_GET['productimage']);
    global $wpdb;
    $sql = $wpdb->get_results("SELECT *
        FROM wp_posts
        WHERE wp_posts.post_parent = '$id' ");
    echo $sql[0]->guid;
}

if(isset($_POST['prescription'])){
    global $wpdb;

    echo $_POST['fname'];

//
//    $insert_api = array(
//        'post_title' => '',
//        'post_name' => '',
//        'post_content' =>  '',
//        'post_status' => 'publish',
//        'post_content_filtered' => '',
//        'post_date' => date('Y-m-d H:i:s'),
//        'post_type' => 'prescription',
//    );
//    $insert = wp_insert_post( $insert_api);
//    print_r($insert);
}

if(isset($_GET['page']))
{
  $id = sanitize_user($_GET['page']);
  global $wpdb;
  $sql = $wpdb->get_results("SELECT *
          FROM wp_posts,wp_postmeta
          WHERE  wp_posts.ID = wp_postmeta.post_id
          AND wp_postmeta.meta_key = 'mobile'
          AND wp_posts.post_status = 'publish'
          AND wp_postmeta.meta_value = 1
          AND wp_posts.ID = ".$id."
          ");
foreach ($sql as $key => $value) {
  // code...
  echo "<h1>".$value->post_title."</h1>";
  echo "<i>".$value->post_date."</i>";
  echo "<div>".$value->post_content."</div>";
}

}
