<?php
require_once("./include/settings.php");

$args = array(
    'numberposts' => -1,
    'orderby' => 'menu_order',
    'order' => 'ASC',
    'post_type' => 'nav_menu_item',
    'meta_query' => array(
		array(
			'key'     => '_menu_item_menu_item_parent',
			'compare' => 'LIKE' // WP 3.5 +
		)
	)
);
$staff = get_posts($args);
print_r($staff);

