<?php

/**
 * Plugin Name: alln1api
 * Plugin URI:  
 * Version:     1.0
 * Description: <a target='_blank' href='../wp-content/plugins/alln1api/readme.html'> ReadMe! </a>
 * Author:      <a target='_blank' href='http://gregoriobalonzo.ml'>http://gregoriobalonzo.ml</a>
 * Author URI:  
 * License:     GPL2
 * License URI: https://www.gnu.org/licenses/gpl-2.0.html
 * Text Domain: gregoriobalonzo.ml
 */

date_default_timezone_set("Asia/Manila"); 
define('ALLNONEAPI',plugin_dir_path(__FILE__));
define('PLUGINNAME','alln1api');
define('PLUGINURL','/wp-content/plugins/alln1api');
 
 
  
 
/**
 * Register a custom menu page.
 * 
 * 
 */
 function alln1api_add_menu(){
     add_menu_page( 
         __( 'ALL-API'),
         'alln1api-sql',
         'manage_options',
         'alln1api',
         'alln1api_home_menu'
     );  
     
     add_menu_page( 
         __( 'Create'),
         'Create',
         'manage_options',
         'alln1create',
         'alln1api_create_menu'
     );     
  
     add_menu_page( 
         __( 'ALL-API-Edit'),
         'ALL-API-Edit',
         'manage_options',
         'alln1edit',
         'alln1api_edit_menu'
     );       
     
 }
 
 
 /**
  * Hide admin mune but can visit
  *
  */
 function hide_menu(){
 	remove_menu_page('alln1create');
 	remove_menu_page('alln1edit');
 }
 
 add_action( 'admin_menu', 'alln1api_add_menu' );
 add_action( 'admin_init', 'hide_menu' );
 
 

/**   
 * Display a custom menu page
 * 
 * 
 */
function alln1api_home_menu(){
	include trailingslashit( dirname(__FILE__) ).'pages/home.php';
}
function alln1api_create_menu() {
	include trailingslashit( dirname(__FILE__) ).'pages/create.php';
}
 
function alln1api_edit_menu() {
    include trailingslashit( dirname(__FILE__) ).'pages/edit.php';
}


 




function my_awesome_func() {

    $username = sanitize_user($_GET['username']);
    $password = sanitize_user(trim($_GET['password']));
 
 
    $user = apply_filters( 'authenticate', null, $username, $password );
 
    if ( $user == null ) {
        // TODO what should the error message be? (Or would these even happen?)
        // Only needed if all authentication handlers fail to return anything.
        $user = new WP_Error( 'authentication_failed', __( '<strong>ERROR</strong>: Invalid username, email address or incorrect password.' ) );
    }
 
    $ignore_codes = array( 'empty_username', 'empty_password' );
 
    if ( is_wp_error( $user ) && ! in_array( $user->get_error_code(), $ignore_codes ) ) {
        /**
         * Fires after a user login has failed.
         *
         * @since 2.5.0
         * @since 4.5.0 The value of `$username` can now be an email address.
         *
         * @param string $username Username or email address.
         */
        do_action( 'wp_login_failed', $username );
    }
 
   
$username = $user->user_login;
$users = get_user_by('login', $username );

// Redirect URL //
if ( !is_wp_error( $users ) )
{
    wp_clear_auth_cookie();
    wp_set_current_user ( $user->ID );
    wp_set_auth_cookie  ( $user->ID );

    //return 1;
    $redirect_to = user_admin_url();
    if($users != ''){
        return 0;
    }else{
        return 1;
    }
    //wp_safe_redirect( $redirect_to );
    exit();
} 


}


add_action(
    'rest_api_init',
    function () {
        register_rest_route(
            'api',
            'login',
            array(
                'methods'  => 'GET',
                'callback' => 'my_awesome_func',
            )
        );
    }
);






function create_order() {
    global $woocommerce;

    $id = sanitize_user($_GET['id']);
    $userid = sanitize_user($_GET['userid']);


    $order_data = array(
        'status' => apply_filters('woocommerce_default_order_status', 'processing'),
        'customer_id' => $userid
    );
    $address = array(
        'first_name' => '111Joe',
        'last_name'  => 'Conlin',
        'company'    => 'Speed Society',
        'email'      => 'joe@testing.com',
        'phone'      => '760-555-1212',
        'address_1'  => '123 Main st.',
        'address_2'  => '104',
        'city'       => 'San Diego',
        'state'      => 'Ca',
        'postcode'   => '92121',
        'country'    => 'US'
    );
  $order = wc_create_order($order_data);

  // The add_product() function below is located in /plugins/woocommerce/includes/abstracts/abstract_wc_order.php
  $order->add_product( get_product( $id ),1 ); // This is an existing SIMPLE product$order->set_address( $address, 'billing' );
  $order->calculate_totals();
  $order->update_status("Pending", 'Imported order', TRUE);
  return 1;
}

  add_action(
    'rest_api_init',
    function () {
        register_rest_route(
            'api',
            'order',
            array(
                'methods'  => 'GET',
                'callback' => 'create_order',
            )
        );
    }
);


//add user customfield
//add_user_meta( 5, 'health_name', 'health name',true);
//add_user_meta( 5, 'health_img', 'link to image',true);
//add_user_meta( 5, 'health_info', 'description',true);
//$user = get_user_meta( 5, 'health_name' );
//print_r($user );

