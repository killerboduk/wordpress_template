

function my_awesome_func() {

    $username = sanitize_user($_GET['username']);
    $password = sanitize_user(trim($_GET['password']));
 
    /**
     * Filters whether a set of user login credentials are valid.
     *
     * A WP_User object is returned if the credentials authenticate a user.
     * WP_Error or null otherwise.
     *
     * @since 2.8.0
     * @since 4.5.0 `$username` now accepts an email address.
     *
     * @param null|WP_User|WP_Error $user     WP_User if the user is authenticated.
     *                                        WP_Error or null otherwise.
     * @param string                $username Username or email address.
     * @param string                $password User password
     */
    $user = apply_filters( 'authenticate', null, $username, $password );
 
    if ( $user == null ) {
        // TODO what should the error message be? (Or would these even happen?)
        // Only needed if all authentication handlers fail to return anything.
        $user = new WP_Error( 'authentication_failed', __( '<strong>ERROR</strong>: Invalid username, email address or incorrect password.' ) );
    }
 
    $ignore_codes = array( 'empty_username', 'empty_password' );
 
    if ( is_wp_error( $user ) && ! in_array( $user->get_error_code(), $ignore_codes ) ) {
        /**
         * Fires after a user login has failed.
         *
         * @since 2.5.0
         * @since 4.5.0 The value of `$username` can now be an email address.
         *
         * @param string $username Username or email address.
         */
        do_action( 'wp_login_failed', $username );
    }
 
   
$username = $user->user_login;
$users = get_user_by('login', $username );

// Redirect URL //
if ( !is_wp_error( $users ) )
{
    wp_clear_auth_cookie();
    wp_set_current_user ( $user->ID );
    wp_set_auth_cookie  ( $user->ID );

    //return 1;
    $redirect_to = user_admin_url();
    if($users != ''){
        return 0;
    }else{
        return 1;
    }
    //wp_safe_redirect( $redirect_to );
    exit();
} 


}


add_action(
    'rest_api_init',
    function () {
        register_rest_route(
            'api',
            'login',
            array(
                'methods'  => 'GET',
                'callback' => 'my_awesome_func',
            )
        );
    }
);






function create_order() {
  $id = sanitize_user($_GET['id']);
  global $woocommerce;
  $user = get_current_user_id();

  $address = array(
      'first_name' => 'Joe',
      'last_name'  => 'Conlin',
      'company'    => 'Speed Society',
      'email'      => 'joe@testing.com',
      'phone'      => '760-555-1212',
      'address_1'  => '123 Main st.',
      'address_2'  => '104',
      'city'       => 'San Diego',
      'state'      => 'Ca',
      'postcode'   => '92121',
      'country'    => 'US'
  );

  // Now we create the order
  $order = wc_create_order();

  // The add_product() function below is located in /plugins/woocommerce/includes/abstracts/abstract_wc_order.php
  $order->add_product( get_product( $id ), $user ); // This is an existing SIMPLE product
  $order->set_address( $address, 'billing' );
  //
  $order->calculate_totals();
  $order->update_status("Completed", 'Imported order', TRUE);
  return 1;
}

  add_action(
    'rest_api_init',
    function () {
        register_rest_route(
            'api',
            'order',
            array(
                'methods'  => 'GET',
                'callback' => 'create_order',
            )
        );
    }
);

