<?php
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Headers: *");
header('Access-Control-Max-Age: 1000');
header("Access-Control-Allow-Methods: GET, POST");
// ini_set('display_errors', 1);
// ini_set('display_startup_errors', 1);
// error_reporting(E_ALL);
?>
<?php

require_once("../../../../wp-load.php");
global $wpdb;







?>
<div class="panel-body">

    <div class="form-group">
        <label class="col-md-3 col-xs-12 control-label">Name</label>
        <div class="col-md-6 col-xs-12">
            <div class="input-group">
                <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                <input type="text" class="form-control" name="health_name">
            </div>
        </div>
    </div>

    <div class="form-group">
        <label class="col-md-3 col-xs-12 control-label">Description</label>
        <div class="col-md-6 col-xs-12">
            <textarea class="form-control" rows="5"></textarea>
            <span class="help-block">Default textarea field</span>
        </div>
    </div>

    <div class="form-group">
        <label class="col-md-3 col-xs-12 control-label">Date</label>
        <div class="col-md-6 col-xs-12">
            <div class="input-group">
                <span class="input-group-addon"><span class="fa fa-calendar"></span></span>
                <input type="text" class="form-control datepicker" value="2014-11-01">
            </div>
            <span class="help-block">Click on input field to get datepicker</span>
        </div>
    </div>




</div>


<div class="panel panel-default">
    <div class="panel-heading ui-draggable-handle">
        <h3 class="panel-title">Notification</h3>
    </div>
    <div class="panel-body scroll mCustomScrollbar _mCS_4 mCS-autoHide mCS_no_scrollbar" >
                <ul class="list-group border-bottom">
                    <li class="list-group-item active" onclick="get_health();"> Quisque lacinia sem ligula, eget #varius massa vulputate at. Sed imperdiet congue enim. Quisque lacinia sem ligula, eget #varius massa vulputate at. Sed imperdiet congue enim. <br><span class="pull-right"><span class="text-muted"><i class="fa fa-clock-o"></i> 14:15 Today</span></span><br></li>
                    <li class="list-group-item" onclick="get_health();"> Quisque lacinia sem ligula, eget #varius massa vulputate at. Sed imperdiet congue enim. Quisque lacinia sem ligula, eget #varius massa vulputate at. Sed imperdiet congue enim. <br><span class="pull-right"><span class="text-muted"><i class="fa fa-clock-o"></i> 14:15 Today</span></span><br></li>
                    <li class="list-group-item" onclick="get_health();"> Quisque lacinia sem ligula, eget #varius massa vulputate at. Sed imperdiet congue enim. Quisque lacinia sem ligula, eget #varius massa vulputate at. Sed imperdiet congue enim. <br><span class="pull-right"><span class="text-muted"><i class="fa fa-clock-o"></i> 14:15 Today</span></span><br></li>
                    <li class="list-group-item" onclick="get_health();"> Quisque lacinia sem ligula, eget #varius massa vulputate at. Sed imperdiet congue enim. Quisque lacinia sem ligula, eget #varius massa vulputate at. Sed imperdiet congue enim. <br><span class="pull-right"><span class="text-muted"><i class="fa fa-clock-o"></i> 14:15 Today</span></span><br></li>
                    <li class="list-group-item" onclick="get_health();"> Quisque lacinia sem ligula, eget #varius massa vulputate at. Sed imperdiet congue enim. Quisque lacinia sem ligula, eget #varius massa vulputate at. Sed imperdiet congue enim. <br><span class="pull-right"><span class="text-muted"><i class="fa fa-clock-o"></i> 14:15 Today</span></span><br></li>
                 </ul>
            </div>
</div>
