<?php
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Headers: *");
header('Access-Control-Max-Age: 1000');
header("Access-Control-Allow-Methods: GET, POST");
 ini_set('display_errors', 1);
 ini_set('display_startup_errors', 1);
 error_reporting(E_ALL);
?>
<?php

require_once("../../../../wp-load.php");
global $wpdb;

$api_query = $wpdb->get_results( "SELECT *
FROM wp_term_taxonomy,wp_term_relationships,wp_posts
WHERE wp_term_taxonomy.taxonomy = 'category'
AND wp_term_relationships.term_taxonomy_id = wp_term_taxonomy.term_id
AND wp_posts.ID = wp_term_relationships.object_id
AND wp_term_taxonomy.description = 'flyers'
AND wp_posts.post_status = 'publish'");
//print_r($query->posts);

foreach ($api_query as $value) {
    ?>
    <div class="post-item">
        <div class="post-title">
            <h3 style="text-transform: uppercase;"><?php echo $value->post_name; ?></h3>
        </div>
        <div class="post-date"><span class="fa fa-calendar"></span><?php echo $value->post_date; ?> </div>
        <div class="post-text">
            <br>
            <?php echo $value->post_content; ?>
        </div>
    </div>
    <?php
    }
    ?>
