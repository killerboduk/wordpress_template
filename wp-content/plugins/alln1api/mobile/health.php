<?php
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Headers: *");
header('Access-Control-Max-Age: 1000');
header("Access-Control-Allow-Methods: GET, POST");
// ini_set('display_errors', 1);
// ini_set('display_startup_errors', 1);
// error_reporting(E_ALL);
?>
<?php

require_once("../../../../wp-load.php");
global $wpdb;
?>



<!-- Button trigger modal -->
<button type="button" class="btn btn-info btn-block" data-toggle="modal" data-target="#exampleModal">
    Add Health Record
</button>


<br>
        <ul class="list-group border-bottom">
            <li class="list-group-item" onclick="get_health();">Health 001</li>
            <li class="list-group-item" onclick="get_health();">Health 002</li>
            <li class="list-group-item" onclick="get_health();">Health 003</li>
            <li class="list-group-item" onclick="get_health();">Health 004</li>
        </ul>




<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Add Health Record</h5>

            </div>
            <div class="modal-body">

                <div class="form-group">
                    <label class="col-md-3 col-xs-12 control-label">Records title</label>
                    <div class="col-md-6 col-xs-12">
                        <div class="input-group">
                            <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                            <input type="text" class="form-control" name="health_title">
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-3 col-xs-12 control-label">Records description</label>
                    <div class="col-md-6 col-xs-12">
                        <textarea class="form-control" rows="5"></textarea>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-3 col-xs-12 control-label">Update photo</label>
                    <div class="col-md-6 col-xs-12">
                        <a class="file-input-wrapper btn btn-default  fileinput btn-info"><span>Browse file</span>
                            <input type="file" class="fileinput btn-info" name="filename" id="filename" title="Browse file" style="left: -122px; top: 19px;"></a>
                        <span class="help-block">Input type file</span>
                    </div>
                </div>
                <br>

                <button type="button" class="btn btn-info btn-block" >
                   Save
                </button>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>


