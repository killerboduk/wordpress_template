<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'wp001' );

/** MySQL database username */
define( 'DB_USER', 'user' );

/** MySQL database password */
define( 'DB_PASSWORD', '!Passw0rd!' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'e)tG@!])y1y&T%uo<1Hf:oIMYYUXc;*>A{#[Y4M_Dpkxo9M#o/onU%}kdJzeS:cc' );
define( 'SECURE_AUTH_KEY',  '3AFbo10[kLfM$ycp)|,OG;54w[Lyd.p$xyA)keWzSYK%hS|,,4YI=_xHe~sBWLdP' );
define( 'LOGGED_IN_KEY',    'w3#l0T6} 34q6CHS4Z+!!C_M,ZIj{ir[hAZ1sgt0-Wn3oELhmg{dF8rh=_5kF?^l' );
define( 'NONCE_KEY',        '^.X0d-X~f-ZnLkKn2]b^xoSF@0nT|uS1L1hD[vY@EDqLV5?i=]h*|3{I!LHfS)}K' );
define( 'AUTH_SALT',        'Z35RV+Xo**HTh#S{VXd2&Xvm:}$R6TCwgizP>8ImO`]1T4>`?+v[r69w;0dTA=*^' );
define( 'SECURE_AUTH_SALT', ',k2zuEvmOzD#9>5WEII)|q%I=Av(.!,bck(9m.CeBJbU#c;WwRx^t)Tw1~&E%4g.' );
define( 'LOGGED_IN_SALT',   '5D^+1qa(1?wEb>Gu(UfuT KH+I,*||$bn);Elw9f/dD:zrI] xu$0m~_Y_PP;gvk' );
define( 'NONCE_SALT',       '-a^bagtv|SiK%g(^{$J7Gp8|vhe!ZCa8X60*v++f,9}h3!}_!:Vhke6e*?(pF>gz' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once( ABSPATH . 'wp-settings.php' );
